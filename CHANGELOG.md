# 1.1.0 (2022-01-11)


### Features

* **script:** add script cicd 43e31fd
* **script:** add script deploy 61c32e5
* **TICKET-123:** Add test option a5016c1

## 1.0.2 (2022-01-11)


### Bug Fixes

* **oops:** good thing I have noticed this 68fd981

## 1.0.1 (2022-01-11)


### Bug Fixes

* **test:** script e3ac9b1
* **test:** script 6422111

# 1.0.0 (2022-01-11)


### Bug Fixes

* **cicd:** script 74434c5
* **cicd:** script 18a1bc3
* **version:** edit f6d640c
